section .text

global exit
global string_length
global print_string
global print_string_to_stderr
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
        mov rax, 60 ; системный вызов функции close
        syscall
        ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax ; 0 в rax
    .loop: ; поиск нуль-терминатора
        mov cl, [rdi] ; текущий символ
        test cl, cl ; проверка символа на нуль
        jz .end
        inc rdi ; следующий символ
        inc rax ; счётчик символов инкрементируется
        jmp .loop
    	.end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        mov rsi, rdi ; адрес строки в rsi
        call string_length
        mov rdx, rax ; длина строки в rdx
        mov rdi, 1 ; дескриптор stdout
        mov rax, 1 ; системный вызов функции write
        syscall
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_string_to_stderr:
		mov rsi, rdi
		call string_length
		mov rdx, rax
		mov rdi, 2
		mov rax, 1
		syscall
		ret
		call print_newline

; Принимает код символа и выводит его в stdout
print_char:
        push rdi ; код символа в стек
        mov rdx, 1 ; длина строки
        mov rsi, rsp ; адрес символа
        mov rdi, 1 ; дескриптор stdout
        mov rax, 1 ; системный вызов функции write
        syscall
        pop rdi    
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rdi, 10
        jmp print_char
        ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov rax, rdi ; число для деления
        mov rdi, rsp ; указатель на стек
        push 0 ; последний символ строки
        sub rsp, 16 
        dec rdi 
        mov r8, 10 ; основание
    .loop:
        xor rdx, rdx
        div r8 ; целая часть в rax, остаток в rdx
        or rdx, 48 
        dec rdi
        mov [rdi], dl ; записываем в стек младший байт rdx
        test rax, rax ; закончилось ли число
        jnz .loop
        call print_string
        add rsp, 24
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        test rdi, rdi ; проверка на знак
        jns .print ; если положительное, выводим как беззнаковое, используя print_uint
        push rdi
        mov rdi, '-' 
        call print_char ; печать минуса
        pop rdi
        neg rdi ; меняем знак и печатаем
    .print:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor rcx, rcx ; для организации цикла
        xor r8, r8 ; для хранения строк
        xor r9, r9
        .loop: ; посимвольно сравниваем 2 строки
            mov r8b, byte[rdi + rcx] 
            mov r9b, byte[rsi + rcx] 
            cmp r8b, r9b 
            jne .not_equals ; символы не совпали
            test r8b, r8b
            je .equals ; полное совпадение 
            inc rcx
            jmp .loop
        .equals:
            mov rax, 1   
            ret
        .not_equals:
            mov rax, 0 
            ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        mov rdi, 0 ; дескриптор stdin
        mov rax, 0 ; системный вызов функции read
        push 0
        mov rsi, rsp ; адрес символа
        mov rdx, 1
        syscall
        pop rax 
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        xor rcx, rcx
        xor r8, r8
    .loop:
        push rcx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rcx
        cmp rax, 0x20 ; проверка на пробелы
        je .spaces
        cmp rax, 0x9
        je .spaces
        cmp rax, 0xA
        je .spaces
        test rax, rax
        je .success
        inc rcx
        cmp rcx, rsi 
        je .error
        inc r8
        mov [rdi + rcx - 1], rax
        jmp .loop
    .spaces:
    	cmp r8, 0
        je .loop
        mov [rdi + rcx], byte 0
    .success:
        mov rax, rdi
        mov rdx, rcx
        ret
    .error:
        xor rax, rax
        xor rdx, rdx
        ret

read_string:
		xor rax, rax
		xor rcx, rcx
	.loop:
		push rcx
		push rdi
		push rsi
		call read_char
		pop rsi
		pop rdi
		pop rcx
		test rax, rax
		je .success
		cmp rax, 0xA
		je .success
		inc rcx
		cmp rcx, rsi
		je .overflow
		mov [rdi + rcx - 1], rax
		jmp .loop
	.success:
		mov [rdi + rcx], byte 0
		mov rax, rdi
		mov rdx, rcx
		jmp .end
	.overflow:
		xor rax, rax
	.end:
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor r8, r8 ; счётчик длины
        mov r10, 10 ; множитель
    .loop:
        xor r9, r9
        cmp byte[rdi+r8], '0'
        jl .check
        cmp byte[rdi+r8], '9'
        jg .check
        cmp byte[rdi+r8], 0
        je .check
        mov r9b, byte[rdi+r8]
        sub r9b, 48
        mul r10
        add rax, r9
        inc r8
        jmp .loop
    .check:
        cmp r8, 0                   
        je .end                
        mov rdx, r8
        ret
    .end:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        cmp byte[rdi], '-' ; если не отрицательное, просто читаем как беззнаковое 
        jne parse_uint
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:   
        push rdi
        call string_length
        pop rdi
        cmp rax, rdx ; проверка длины
        jg .error
        xor r8, r8 ; счетчик длины
    .loop:
        mov rax, 0
        mov al, byte[rdi + r8];
        mov byte[rsi + r8], al;
        test rax, rax
        je .end
        inc r8
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        mov rax, r8
        ret
