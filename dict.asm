%include "lib.inc"
%define NUM 8
%define ZERO 0
global find_word

section .text

find_word:
    .loop:
    	test rsi, rsi 
    	je .not_found
    	add rsi, NUM
	push rsi
	push rdi
    	call string_equals
	pop rdi
	pop rsi
    	sub rsi, NUM 
	test rax, rax
    	jnz .found
    	mov rsi, [rsi] 
    	jmp .loop
	.found:
		mov rax, rsi
		ret
	.not_found:
		ret