%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%define BUF_SIZE 255
%define ZERO 0
%define NUM 8

extern find_word

global _start

section .rodata

not_found:
	db "Ключ не найден", ZERO
overflow:
	db "Строка слишком большая", ZERO

section .bss    

buffer: 
    resb BUF_SIZE

section .text

_start:
	mov rdi, buffer
	push rdi
	call read_string
    pop rdi
	test rax, rax
	jz .buf_overflow
	
	mov rdi, rax
	mov rsi, pointer
	call find_word
	test rax, rax
	jz .not_found


    mov rdi, rax
    add rdi, NUM
	push rdi
	call string_length
	inc rax
	pop rdi
	add rdi, rax
	call print_string
	call print_newline
	call exit

	.buf_overflow:
		mov rdi, overflow
		jmp print_string_to_stderr
	
	.not_found:
		mov rdi, not_found
		jmp print_string_to_stderr
